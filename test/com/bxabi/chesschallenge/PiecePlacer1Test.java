package com.bxabi.chesschallenge;

import org.junit.Assert;
import org.junit.Test;

public class PiecePlacer1Test {
	
	@Test
	public void canPlaceTest() {
		// Kings, Queens, Bishops, Rooks, Knights
		PiecePlacer1 placer = new PiecePlacer1(3, 3, new int[] { 2, 0, 0, 1, 0 }); // 2K,1R
		Assert.assertEquals(4, placer.getSolutionCount());

		placer = new PiecePlacer1(4, 4, new int[] { 0, 0, 0, 2, 4 }); // 2R,4N
		Assert.assertEquals(8, placer.getSolutionCount());

		placer = new PiecePlacer1(3, 3, new int[] { 3, 0, 0, 0, 1 }); // 3K,1N
		Assert.assertEquals(4, placer.getSolutionCount());

		placer = new PiecePlacer1(3, 3, new int[] { 0, 0, 0, 3, 0 }); // 3R
		Assert.assertEquals(6, placer.getSolutionCount());

		placer = new PiecePlacer1(3, 3, new int[] { 1, 0, 3, 0, 0 }); // 1K,3B
		Assert.assertEquals(4, placer.getSolutionCount());

		placer = new PiecePlacer1(4, 4, new int[] { 0, 2, 3, 0, 0 }); // 2Q, 3B
		Assert.assertEquals(8, placer.getSolutionCount());

		placer = new PiecePlacer1(8, 8, new int[] { 0, 8, 0, 0, 0 }); // 8Q
		Assert.assertEquals(92, placer.getSolutionCount());

		// placer = new PiecePlacer(12, 12, new int[] { 0, 13, 0, 0, 0 });
		// Assert.assertEquals(14200, placer.getSolutionCount());
	}
}
