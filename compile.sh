mkdir build
javac -d build src/com/bxabi/chesschallenge/*.java
cd build/
jar cfev ../chess1.jar com.bxabi.chesschallenge.ChessChallenge1 com/bxabi/chesschallenge/*.class
jar cfev ../chess2.jar com.bxabi.chesschallenge.ChessChallenge2 com/bxabi/chesschallenge/*.class
cd ..
