package com.bxabi.chesschallenge;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class PiecePlacer1 extends AbstractPiecePlacer {
	public PiecePlacer1(int m, int n, int[] pieceCounts) {
		super(m, n, pieceCounts);
	}

	protected void placeNextPiece(int x, int y) {
		LinkedList<Placement> solution = new LinkedList<Placement>();
		Placement placement = new Placement(x, y, pieces.get(0));
		solution.add(placement);
		placePiece(solution);
	}

	private void placePiece(LinkedList<Placement> placed) {
		if (placed.size() == pieces.size()) {
			solutionCount.incrementAndGet();
			printSolution(placed);
			return;
		}

		ChessPiece nextPiece = pieces.get(placed.size());
		int startx = 0;
		int starty = 0;
		// if the new piece is the same type as the previous one, we only try to
		// place it after the last one, not from the beginning of the board.
		if (!placed.isEmpty()) {
			Placement last = placed.getLast();
			if (last.piece.equals(nextPiece)) {
				startx = last.x;
				starty = last.y;
			}
		}

		for (int i = startx; i < m; i++) {
			for (int j = starty; j < n; j++) {
				Placement placement = new Placement(i, j, nextPiece);
				if (canPlace(placement, placed)) {
					LinkedList<Placement> solution = new LinkedList<Placement>();
					solution.addAll(placed);
					solution.add(placement);
					placePiece(solution);
				}
			}
			starty = 0;
		}
	}

	private boolean canPlace(Placement newP, List<Placement> placed) {
		// canplacetest.incrementAndGet();
		for (Placement p : placed) {
			if (p.x == newP.x && p.y == newP.y) {
				return false;
			}
			if (attacks(p, newP)) {
				return false;
			}
			// if the pieces are the same type, one-way attack verification is
			// enough
			if (!p.piece.equals(newP.piece)) {
				if (attacks(newP, p)) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean attacks(Placement p, Placement newP) {
		switch (p.piece) {
		case QUEEN:
			if (newP.x == p.x || newP.y == p.y || p.x - newP.x == p.y - newP.y || p.x - newP.x == -(p.y - newP.y)) {
				return true;
			}
			break;
		case ROOK:
			if (newP.x == p.x || newP.y == p.y) {
				return true;
			}
			break;
		case BISHOP:
			if (p.x - newP.x == p.y - newP.y || p.x - newP.x == -(p.y - newP.y)) {
				return true;
			}
			break;
		case KING:
			if (Math.abs(p.x - newP.x) <= 1 && Math.abs(p.y - newP.y) <= 1) {
				return true;
			}
			break;
		case KNIGHT:
			if ((Math.abs(p.x - newP.x) == 1 && Math.abs(p.y - newP.y) == 2)
					|| (Math.abs(p.x - newP.x) == 2 && Math.abs(p.y - newP.y) == 1)) {
				return true;
			}
			break;
		}
		return false;
	}

	private void printSolution(List<Placement> placed) {
		StringBuilder builder = new StringBuilder();
		ChessPiece[][] board = new ChessPiece[m][n];
		for (Placement p : placed) {
			board[p.x][p.y] = p.piece;
		}

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (board[i][j] != null) {
					builder.append(board[i][j].getShortName());
				} else {
					builder.append(" ");
				}
			}
			builder.append("\n");
		}
		builder.append("\n");

		synchronized (out) {
			try {
				out.write(builder.toString());
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
