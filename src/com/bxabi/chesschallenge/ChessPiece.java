package com.bxabi.chesschallenge;

public enum ChessPiece {
	// King, Queen, Bishop, Rook and Knight
	QUEEN(2, 'Q'), ROOK(4, 'R'), BISHOP(3, 'B'), KING(1, 'K'), KNIGHT(5, 'N');

	private final int value;
	private final char shortName;

	ChessPiece(final int newValue, final char shortName) {
		value = newValue;
		this.shortName = shortName;
	}

	public int getValue() {
		return value;
	}

	public char getShortName() {
		return shortName;
	}
}
