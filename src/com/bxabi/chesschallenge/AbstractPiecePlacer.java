package com.bxabi.chesschallenge;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public abstract class AbstractPiecePlacer {
	protected int m;
	protected int n;

	protected List<ChessPiece> pieces;
	protected AtomicLong solutionCount = new AtomicLong();

	protected static BufferedWriter out = new BufferedWriter(new OutputStreamWriter(System.out));

	public AbstractPiecePlacer(int m, int n, int[] pieceCounts) {
		this.m = m;
		this.n = n;

		pieces = new ArrayList<>();
		addToPieceList(pieceCounts, ChessPiece.QUEEN);
		addToPieceList(pieceCounts, ChessPiece.ROOK);
		addToPieceList(pieceCounts, ChessPiece.BISHOP);
		addToPieceList(pieceCounts, ChessPiece.KING);
		addToPieceList(pieceCounts, ChessPiece.KNIGHT);

		long start = System.currentTimeMillis();

		int cpus = Runtime.getRuntime().availableProcessors();
		int maxThreads = cpus - 1;
		if (maxThreads <= 0) {
			maxThreads = 1;
		}
		BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(m * n);
		ThreadPoolExecutor executor = new ThreadPoolExecutor(maxThreads, maxThreads, 1, TimeUnit.DAYS, workQueue);

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				Thread thread = new PlacerThread(i, j);
				executor.submit(thread);
			}
		}
		executor.shutdown();
		try {
			executor.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		long end = System.currentTimeMillis();

		try {
			out.write(solutionCount + " solutions found.\n");
			out.write(end - start + " miliseconds.\n");
			// out.write(canplacetest + " positions tested.\n");
			out.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void addToPieceList(int[] pieceCounts, ChessPiece piece) {
		for (int j = 0; j < pieceCounts[piece.getValue() - 1]; j++) {
			pieces.add(piece);
		}
	}

	private class PlacerThread extends Thread {
		private int x;
		private int y;

		public PlacerThread(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public void run() {
			placeNextPiece(x, y);
		}
	}

	protected abstract void placeNextPiece(int x, int y);

	public long getSolutionCount() {
		return solutionCount.get();
	}
}
