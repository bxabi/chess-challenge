package com.bxabi.chesschallenge;

public class ChessChallengeRunner {
	public static void execute(String[] args, int version) {
		if (args.length < 7) {
			System.err.println("Please give the parameters in the following order:");
			System.err.println("M, N, Kings, Queens, Bishops, Rooks, Knights");
			System.exit(1);
		}

		int m = readArg(args, 0);
		int n = readArg(args, 1);

		int[] pieces = new int[5];
		for (int i = 0; i < 5; i++) {
			pieces[i] = readArg(args, i + 2);
		}

		if (version == 1) {
			new PiecePlacer1(m, n, pieces);
		} else {
			new PiecePlacer2(m, n, pieces);
		}
	}

	private static int readArg(String[] args, int i) {
		try {
			int result = Integer.parseInt(args[i]);
			return result;
		} catch (NumberFormatException e) {
			throw new RuntimeException("Invalid number given in the parameter list");
		}
	}
}
