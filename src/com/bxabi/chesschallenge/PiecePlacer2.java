package com.bxabi.chesschallenge;

import java.io.IOException;
import java.util.HashMap;

public class PiecePlacer2 extends AbstractPiecePlacer {

	private static HashMap<Integer, Character> mapper = mapPieces();

	public PiecePlacer2(int m, int n, int[] pieceCounts) {
		super(m, n, pieceCounts);
	}

	private static HashMap<Integer, Character> mapPieces() {
		HashMap<Integer, Character> mapper = new HashMap<>();
		for (ChessPiece p : ChessPiece.values()) {
			mapper.put(p.getValue(), p.getShortName());
		}
		return mapper;
	}

	@Override
	protected void placeNextPiece(int x, int y) {
		Placement placement = new Placement(x, y, pieces.get(0));
		int[][] board = new int[m][n];

		placeOnBoard(board, placement);
		placePiece(board, placement, 1);
	}

	private void placeOnBoard(int[][] board, Placement p) {
		if (p.piece.equals(ChessPiece.ROOK) || p.piece.equals(ChessPiece.QUEEN)) {
			for (int i = 0; i < m; i++) {
				board[i][p.y] = -1;
			}
			for (int i = 0; i < n; i++) {
				board[p.x][i] = -1;
			}
		}
		if (p.piece.equals(ChessPiece.BISHOP) || p.piece.equals(ChessPiece.QUEEN)) {
			int i = p.x;
			int j = p.y;
			while (i >= 0 && j >= 0) {
				board[i][j] = -1;
				i--;
				j--;
			}
			i = p.x;
			j = p.y;
			while (i < m && j < n) {
				board[i][j] = -1;
				i++;
				j++;
			}
			i = p.x;
			j = p.y;
			while (i >= 0 && j < n) {
				board[i][j] = -1;
				i--;
				j++;
			}
			i = p.x;
			j = p.y;
			while (i < m && j >= 0) {
				board[i][j] = -1;
				i++;
				j--;
			}
		}
		if (p.piece.equals(ChessPiece.KING)) {
			for (int x = p.x - 1; x <= p.x + 1; x++) {
				for (int y = p.y - 1; y <= p.y + 1; y++) {
					addIfOnBoard(board, x, y);
				}
			}
		}
		if (p.piece.equals(ChessPiece.KNIGHT)) {
			int x = p.x - 1;
			int y = p.y - 2;
			addIfOnBoard(board, x, y);
			x = p.x + 1;
			y = p.y + 2;
			addIfOnBoard(board, x, y);
			x = p.x - 1;
			y = p.y + 2;
			addIfOnBoard(board, x, y);
			x = p.x + 1;
			y = p.y - 2;
			addIfOnBoard(board, x, y);

			x = p.x - 2;
			y = p.y - 1;
			addIfOnBoard(board, x, y);
			x = p.x + 2;
			y = p.y + 1;
			addIfOnBoard(board, x, y);
			x = p.x - 2;
			y = p.y + 1;
			addIfOnBoard(board, x, y);
			x = p.x + 2;
			y = p.y - 1;
			addIfOnBoard(board, x, y);
		}
		board[p.x][p.y] = p.piece.getValue();
	}

	private void addIfOnBoard(int[][] board, int i, int j) {
		if (i >= 0 && i < m && j >= 0 && j < n) {
			board[i][j] = -1;
		}
	}

	private void placePiece(int[][] board, Placement last, int count) {
		if (count == pieces.size()) {
			solutionCount.incrementAndGet();
			printSolution(board);
			return;
		}

		ChessPiece nextPiece = pieces.get(count);
		int startx = 0;
		int starty = 0;
		// if the new piece is the same type as the previous one, we only try to
		// place it after the last one, not from the beginning of the board.
		if (count > 0) {
			if (last.piece.equals(nextPiece)) {
				startx = last.x;
				starty = last.y;
			}
		}

		for (int i = startx; i < m; i++) {
			for (int j = starty; j < n; j++) {
				if (board[i][j] == 0) {
					Placement placement = new Placement(i, j, nextPiece);
					if (!attacks(board, placement)) {
						int[][] newBoard = new int[m][n];
						for (int x = 0; x < m; x++) {
							for (int y = 0; y < n; y++) {
								newBoard[x][y] = board[x][y];
							}
						}
						placeOnBoard(newBoard, placement);
						placePiece(newBoard, placement, count + 1);
					}
				}
			}
			starty = 0;
		}
	}

	private boolean attacks(int[][] board, Placement p) {
		if (p.piece.equals(ChessPiece.ROOK) || p.piece.equals(ChessPiece.QUEEN)) {
			for (int i = 0; i < m; i++) {
				if (board[i][p.y] > 0) {
					return true;
				}
			}
			for (int i = 0; i < n; i++) {
				if (board[p.x][i] > 0) {
					return true;
				}
			}
		}
		if (p.piece.equals(ChessPiece.BISHOP) || p.piece.equals(ChessPiece.QUEEN)) {
			int i = p.x;
			int j = p.y;
			while (i >= 0 && j >= 0) {
				if (board[i][j] > 0) {
					return true;
				}
				i--;
				j--;
			}
			i = p.x;
			j = p.y;
			while (i < m && j < n) {
				if (board[i][j] > 0) {
					return true;
				}
				i++;
				j++;
			}
			i = p.x;
			j = p.y;
			while (i >= 0 && j < n) {
				if (board[i][j] > 0) {
					return true;
				}
				i--;
				j++;
			}
			i = p.x;
			j = p.y;
			while (i < m && j >= 0) {
				if (board[i][j] > 0) {
					return true;
				}
				i++;
				j--;
			}
		}
		if (p.piece.equals(ChessPiece.KING)) {
			for (int x = p.x - 1; x <= p.x + 1; x++) {
				for (int y = p.y - 1; y <= p.y + 1; y++) {
					if (hasPieceAlready(board, x, y)) {
						return true;
					}
				}
			}
		}
		if (p.piece.equals(ChessPiece.KNIGHT)) {
			int x = p.x - 1;
			int y = p.y - 2;
			if (hasPieceAlready(board, x, y)) {
				return true;
			}
			x = p.x + 1;
			y = p.y + 2;
			if (hasPieceAlready(board, x, y)) {
				return true;
			}
			x = p.x - 1;
			y = p.y + 2;
			if (hasPieceAlready(board, x, y)) {
				return true;
			}
			x = p.x + 1;
			y = p.y - 2;
			if (hasPieceAlready(board, x, y)) {
				return true;
			}

			x = p.x - 2;
			y = p.y - 1;
			if (hasPieceAlready(board, x, y)) {
				return true;
			}
			x = p.x + 2;
			y = p.y + 1;
			if (hasPieceAlready(board, x, y)) {
				return true;
			}
			x = p.x - 2;
			y = p.y + 1;
			if (hasPieceAlready(board, x, y)) {
				return true;
			}
			x = p.x + 2;
			y = p.y - 1;
			if (hasPieceAlready(board, x, y)) {
				return true;
			}
		}
		return false;
	}

	private boolean hasPieceAlready(int[][] board, int i, int j) {
		if (i >= 0 && i < m && j >= 0 && j < n) {
			if (board[i][j] > 0) {
				return true;
			}
		}
		return false;
	}

	private void printSolution(int[][] board) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (board[i][j] > 0) {
					builder.append(mapper.get(board[i][j]));
				} else {
					builder.append(" ");
				}
			}
			builder.append("\n");
		}
		builder.append("\n");

		try {
			out.write(builder.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
