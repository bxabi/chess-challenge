package com.bxabi.chesschallenge;

public class Placement {
	int x;
	int y;
	ChessPiece piece;

	public Placement(int x, int y, ChessPiece piece) {
		this.x = x;
		this.y = y;
		this.piece = piece;
	}

	@Override
	public String toString() {
		return x + "," + y + ":" + piece.name();
	}
}
